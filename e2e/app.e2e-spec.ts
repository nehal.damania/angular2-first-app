import { Angular2FirstApppPage } from './app.po';

describe('angular2-first-appp App', function() {
  let page: Angular2FirstApppPage;

  beforeEach(() => {
    page = new Angular2FirstApppPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
